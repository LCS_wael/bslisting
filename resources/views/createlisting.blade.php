@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Create Listing') }}  <a href="/home" class=" float-right" >Go Back</a></div>

                <div class="card-body">
                    {!! Form::open(['action' => 'ListingController@store', 'method' => 'POST']) !!}
                        {{ Form::bsText('name','',['placeholder' => 'Company Name']) }}
                        {{ Form::bsText('website','',['placeholder' => 'Company Website']) }}
                        {{ Form::bsText('email','',['placeholder' => 'Contact Email']) }}
                        {{ Form::bsText('phone','',['placeholder' => 'Contact Phone']) }}
                        {{ Form::bsText('adress','',['placeholder' => 'Business Adress']) }}
                        {{ Form::bsTextArea('bio','',['placeholder' => 'About This Business']) }}
                        {{ Form::bsSubmit('Submit', ['class' => 'btn btn-primary']) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
